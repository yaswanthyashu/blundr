package yaswanth;

/**
 * Implementing a class to display the users details
 * 
 * @author yashu1
 */
public class User {
	String name;
	int age;
	String proffesion;
	String primaryHobbie;
	// ArrayList<String> hobbies = new ArrayList<String>();
	String hobbies[] = new String[100];

	/**
	 * Created a method with arguments and calling all the instance variables
	 * 
	 * @param name
	 * @param age
	 * @param profession
	 * @param primaryHobbie
	 * @param hobbies
	 */
	public void createNewBlunderUser(String name, int age, String profession, String primaryHobbie, String[] hobbies) {
		this.name = name;
		this.age = age;
		this.proffesion = profession;
		this.primaryHobbie = primaryHobbie;
		this.hobbies = hobbies;
		System.out.println(this);
		// this.hobbies = hobbies;
	}

	@Override
	public String toString() {
		String StringToReturn = "";
		StringToReturn += "----------------User Information------------------" + "\n";
		StringToReturn += "Name                      :" + this.name + "\n";
		StringToReturn += "Age                       :" + this.age + "\n";
		StringToReturn += "Profession                :" + this.proffesion + "\n";
		StringToReturn += "PrimaryHobbie             :" + this.primaryHobbie + "\n";
		
		// code to calculate hobbies
		String hobbyString = "[";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\" "+",";
		}
		hobbyString += "]";
		
		StringToReturn += "Hobbies are               :" + hobbyString + "\n";
		// StringToReturn += "hobbies :"+this.hobbies + "\n";
		return StringToReturn;
	}
}
